package buenservlet;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class FormBean implements Serializable {

	private static final long serialVersionUID = -4545735676707805436L;
	private String name;

	@PostConstruct
	public void init() {
		System.out.println("Trololololó");
	}
	
	public String process() {
		System.out.println(this.name);
		return "test.xhtml";
	}

	public String getName() {
		return this.name;
	}

	public void setName(String value) {
		this.name = value;
	}
}
